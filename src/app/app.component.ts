import {Component, OnInit} from '@angular/core';
import * as me from 'melonjs'
import {Perso} from "./perso";
import {Fleche} from "./fleche";
import {PlayScreen} from "./playscreen";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'melon';

  ngOnInit(): void {
    me.device.onReady(function () {
      // initialize the display canvas once the device/browser is ready
      if (!me.video.init(800, 600, {parent : "screen"})) {
        alert("Your browser does not support HTML5 canvas.");
        return;
      }

      // add a gray background to the default Stage
      me.game.world.addChild(new me.ColorLayer("background", "#202020"));



      // add a font text display object
      // me.game.world.addChild(new me.Text(609, 281, {
      //   font: "Arial",
      //   size: 160,
      //   fillStyle: "#FFFFFF",
      //   textBaseline : "middle",
      //   textAlign : "center",
      //   text : "Hello World !"
      // }));

      me.loader.preload([], () => {
        // set the "Play/Ingame" Screen Object
        me.state.set(me.state.PLAY, new PlayScreen());

        // set the fade transition effect
        me.state.transition("fade","#FFFFFF", 250);

        // register our objects entity in the object pool
        // me.pool.register("perso", Perso);
        // me.pool.pull("perso", 200,200,{height: 100, width: 100})

        // switch to PLAY state
        me.state.change(me.state.PLAY);
      })
    });
  }

}
