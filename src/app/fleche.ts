import * as Melon from 'melonjs'

export class Fleche extends Melon.Renderable {

  polymask: Melon.Polygon;
  color: Melon.Color;
  // constructor
  constructor() {
    super(0, 0, Melon.game.viewport.width, Melon.game.viewport.height);

     this.polymask = new Melon.Polygon(0, 0, [
      {x: 155, y: 77},
      {x: 105, y: 77},
      {x: 80, y: 120},
      {x: 105, y: 163},
      {x: 155, y: 163},
      {x: 180, y: 120}
    ]);

    this.polymask.scale(4.0);

    // a temporary color object
    this.color = new Melon.Color();
  }

  update() {
    return true;
  }

  // draw function
  draw(renderer: Melon.video.Canvas) {
    renderer.clearColor("#FFFFFF");
    renderer.setGlobalAlpha(1.0);
    renderer.setColor("#ffcc00");
    renderer.setGlobalAlpha(0.375);
    this.polymask.rotate(0.05, this.polymask.getBounds().center);
    renderer.setMask(this.polymask);
    renderer.fill(this.polymask.getBounds());
    renderer.clearMask();
  }
}