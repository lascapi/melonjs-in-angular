import * as Melon from 'melonjs'
import {Fleche} from './fleche'
import {Perso} from "./perso";

export class PlayScreen extends Melon.Stage {

  /**
   *  action to perform on state change
   */
  onResetEvent() {

    // disable gravity
    Melon.game.world.gravity.set(0, 0)

    // load a level
    // Melon.level.load("isometric");
    Melon.game.world.addChild(new Fleche())
    Melon.game.world.addChild(new Perso(10,300,{height: 100, width: 20}, 0))
    Melon.game.world.addChild(new Perso(770,300,{height: 100, width: 20}, 1))
  }

  /**
   *  action to perform on state change
   */
  onDestroyEvent() {
    // unsubscribe to all events
    Melon.input.releasePointerEvent("pointermove", Melon.game.viewport)
  }
}
