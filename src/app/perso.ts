import * as Melon from 'melonjs'
import {Renderable} from 'melonjs'

export class Perso extends Melon.Entity {

  body: Melon.Body
  renderable: Renderable
  shape: Melon.Polygon
  dx: number = 0
  dy: number = 0

  constructor(x: number, y: number, settings: any, joystick: number) {
    super(x, y, settings);
    // walking & jumping speed
    this.body.setMaxVelocity(3, 15);
    this.body.setFriction(0.4, 0);


    // Keyboard
    Melon.input.bindKey(Melon.input.KEY.LEFT, "left");
    Melon.input.bindKey(Melon.input.KEY.RIGHT, "right");
    Melon.input.bindKey(Melon.input.KEY.UP, "up");
    Melon.input.bindKey(Melon.input.KEY.DOWN, "down");

    Melon.input.bindKey(Melon.input.KEY.A, "left");
    Melon.input.bindKey(Melon.input.KEY.D, "right");
    Melon.input.bindKey(Melon.input.KEY.W, "up");
    Melon.input.bindKey(Melon.input.KEY.S, "down");

    // Joystick
    // map axes
    Melon.input.bindGamepad(
      joystick,
      {
        type: "axes",
        code: Melon.input.GAMEPAD.AXES.LX,
        threshold: -0.05
      },
      Melon.input.KEY.LEFT
    );

    Melon.input.bindGamepad(
      joystick,
      {
        type: "axes",
        code: Melon.input.GAMEPAD.AXES.LX,
        threshold: 0.05
      },
      Melon.input.KEY.RIGHT
    );
    Melon.input.bindGamepad(
      joystick,
      {
        type: "axes",
        code: Melon.input.GAMEPAD.AXES.LY,
        threshold: -0.05
      },
      Melon.input.KEY.UP
    );
    Melon.input.bindGamepad(
      joystick,
      {
        type: "axes",
        code: Melon.input.GAMEPAD.AXES.LY,
        threshold: 0.05
      },
      Melon.input.KEY.DOWN
    );

    this.shape = new Melon.Rect(x, y, settings.width, settings.height)
    this.renderable = new Melon.Renderable(x, y, settings.width, settings.height)

    Melon.event.on(Melon.event.GAMEPAD_UPDATE,
      (index: any, type: any, button: any, current_value: any, current_pressed: any) => {
        // console.info(index, type, button, current_value, current_pressed, joystick)
        if (index === joystick.toString()) {
          if (button === 0) this.dx = current_value * 5
          if (button === 1) this.dy = current_value * 5
        }
      }
    )
    //   0 axes 0 0.28330332040786743 undefined

  }

  preDraw(renderer: any) {
    renderer.save();
    renderer.setColor("#FF0000");
    renderer.fill(this.shape);
    renderer.setColor("#FFFFFF");
    renderer.restore();
  }

  update(input: number) {
    let x = 0
    let y = 0
    if (Melon.input.isKeyPressed("left")) {
      // this.body.force.x = -10;
      // this.shape.translate(-3,0)
      x = this.dx
    }
    if (Melon.input.isKeyPressed("right")) {
      // this.body.force.x = 10;
      // this.shape.translate(3,0)
      x = this.dx
    }
    if (Melon.input.isKeyPressed("up")) {
      // this.body.force.x = 10;
      // this.shape.translate(0,-3)
      y = this.dy
    }
    if (Melon.input.isKeyPressed("down")) {
      // this.body.force.x = 10;
      // this.shape.translate(0, 3)
      y = this.dy
    }
    this.shape.translate(x, y)
    // this.shape.rotate(x/360)
  }
}
